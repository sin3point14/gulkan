/*
 * gulkan
 * Copyright 2020 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "normal-model-renderer.h"

typedef struct _NormalModelRendererPrivate
{
  GulkanSwapchainRenderer parent;

  GulkanUniformBuffer *transformation_ubo;
  GulkanDescriptorPool *descriptor_pool;

  VkPipeline pipeline;
  VkDescriptorSet descriptor_set;

  VkSampler diffuse_sampler;
  VkSampler normal_sampler;
} NormalModelRendererPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (NormalModelRenderer, normal_model_renderer,
                            GULKAN_TYPE_SWAPCHAIN_RENDERER)

static void
normal_model_renderer_init (NormalModelRenderer *self)
{
  (void) self;
}

static void
_finalize (GObject *gobject)
{
  NormalModelRenderer *self = NORMAL_MODEL_RENDERER (gobject);
  NormalModelRendererPrivate *priv = normal_model_renderer_get_instance_private (self);

  GulkanClient *client = gulkan_renderer_get_client (GULKAN_RENDERER (self));
  VkDevice device = gulkan_client_get_device_handle (client);
  gulkan_device_wait_idle (gulkan_client_get_device (client));

  g_object_unref (priv->descriptor_pool);
  vkDestroyPipeline (device, priv->pipeline, NULL);
  g_object_unref (priv->transformation_ubo);

  G_OBJECT_CLASS (normal_model_renderer_parent_class)->finalize (gobject);
}

static gboolean
_init_texture_sampler (VkDevice device, VkSampler *sampler)
{
  VkSamplerCreateInfo info = {
    .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
    .magFilter = VK_FILTER_LINEAR,
    .minFilter = VK_FILTER_LINEAR,
    .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
    .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
    .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
    .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
    .anisotropyEnable = VK_TRUE,
    .maxAnisotropy = 16,
    .compareEnable = VK_FALSE,
    .compareOp = VK_COMPARE_OP_ALWAYS,
    .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
    .unnormalizedCoordinates = VK_FALSE
  };

  VkResult res = vkCreateSampler (device, &info, NULL, sampler);
  vk_check_error ("vkCreateSampler", res, FALSE);

  return TRUE;
}

static void
_init_draw_cmd (GulkanSwapchainRenderer *renderer,
                VkCommandBuffer          cmd_buffer)
{
  NormalModelRenderer *self = NORMAL_MODEL_RENDERER (renderer);
  NormalModelRendererPrivate *priv = normal_model_renderer_get_instance_private (self);

  NormalModelRendererClass *klass = NORMAL_MODEL_RENDERER_GET_CLASS (self);
  if (klass->init_draw_cmd == NULL)
    return;

  vkCmdBindPipeline (cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                     priv->pipeline);

  VkPipelineLayout layout =
    gulkan_descriptor_pool_get_pipeline_layout (priv->descriptor_pool);

  vkCmdBindDescriptorSets (cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                           layout, 0, 1, &priv->descriptor_set, 0, NULL);

  klass->init_draw_cmd (self, cmd_buffer);
}

static gboolean
_init_pipeline (GulkanSwapchainRenderer *renderer,
                gconstpointer            data)
{
  const ShaderResources *resources = (const ShaderResources*) data;

  NormalModelRenderer *self = NORMAL_MODEL_RENDERER (renderer);
  NormalModelRendererPrivate *priv = normal_model_renderer_get_instance_private (self);

  GulkanClient *client = gulkan_renderer_get_client (GULKAN_RENDERER (self));
  VkDevice device = gulkan_client_get_device_handle (client);

  VkPipelineVertexInputStateCreateInfo vi_create_info = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
    .vertexBindingDescriptionCount = 5,
    .pVertexBindingDescriptions =
      (VkVertexInputBindingDescription[]){
        { .binding = 0,
          .stride = 3 * sizeof (float),
          .inputRate = VK_VERTEX_INPUT_RATE_VERTEX },
        { .binding = 1,
          .stride = 3 * sizeof (float),
          .inputRate = VK_VERTEX_INPUT_RATE_VERTEX },
        { .binding = 2,
          .stride = 3 * sizeof (float),
          .inputRate = VK_VERTEX_INPUT_RATE_VERTEX },
        { .binding = 3,
          .stride = 2 * sizeof (float),
          .inputRate = VK_VERTEX_INPUT_RATE_VERTEX },
        { .binding = 4,
          .stride = 3 * sizeof (float),
          .inputRate = VK_VERTEX_INPUT_RATE_VERTEX },
        },
    .vertexAttributeDescriptionCount = 5,
    .pVertexAttributeDescriptions =
      (VkVertexInputAttributeDescription[]){
        { .location = 0,
          .binding = 0,
          .format = VK_FORMAT_R32G32B32_SFLOAT,
          .offset = 0 },
        { .location = 1,
          .binding = 1,
          .format = VK_FORMAT_R32G32B32_SFLOAT,
          .offset = 0 },
        { .location = 2,
          .binding = 2,
          .format = VK_FORMAT_R32G32B32_SFLOAT,
          .offset = 0 },
        { .location = 3,
          .binding = 3,
          .format = VK_FORMAT_R32G32_SFLOAT,
          .offset = 0 },
        { .location = 4,
          .binding = 4,
          .format = VK_FORMAT_R32G32B32_SFLOAT,
          .offset = 0 } 
        }
  };

  VkShaderModule vs_module;
  if (!gulkan_renderer_create_shader_module (
        GULKAN_RENDERER (self), resources->vert, &vs_module))
    return FALSE;

  VkShaderModule fs_module;
  if (!gulkan_renderer_create_shader_module (
        GULKAN_RENDERER (self), resources->frag, &fs_module))
    return FALSE;

  VkPipelineLayout layout =
    gulkan_descriptor_pool_get_pipeline_layout (priv->descriptor_pool);

  GulkanSwapchainRenderer *scr = GULKAN_SWAPCHAIN_RENDERER (self);
  VkRenderPass pass = gulkan_swapchain_renderer_get_render_pass_handle (scr);

  VkGraphicsPipelineCreateInfo pipeline_info = {
    .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
    .stageCount = 2,
    .pStages =
      (VkPipelineShaderStageCreateInfo[]){
        {
          .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
          .stage = VK_SHADER_STAGE_VERTEX_BIT,
          .module = vs_module,
          .pName = "main",
        },
        {
          .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
          .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
          .module = fs_module,
          .pName = "main",
        },
      },
    .pVertexInputState = &vi_create_info,
    .pInputAssemblyState =
      &(VkPipelineInputAssemblyStateCreateInfo){
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
      },
    .pViewportState = &(VkPipelineViewportStateCreateInfo) {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
      .viewportCount = 1,
      .scissorCount = 1,
    },
    .pDynamicState =
      &(VkPipelineDynamicStateCreateInfo){
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = 2,
        .pDynamicStates =
          (VkDynamicState[]){
            VK_DYNAMIC_STATE_VIEWPORT,
            VK_DYNAMIC_STATE_SCISSOR,
         },
     },
    .pRasterizationState =
      &(VkPipelineRasterizationStateCreateInfo){
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_CLOCKWISE,
        .lineWidth = 1.0f,
      },
    .pMultisampleState =
      &(VkPipelineMultisampleStateCreateInfo){
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
      },
    .pColorBlendState =
      &(VkPipelineColorBlendStateCreateInfo){
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .attachmentCount = 1,
        .blendConstants = { 0.f, 0.f, 0.f, 0.f },
        .pAttachments =
          (VkPipelineColorBlendAttachmentState[]){
            { .colorWriteMask =
                VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
                VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT },
          } },
    .layout = layout,
    .renderPass = pass,
  };

  VkResult res = vkCreateGraphicsPipelines (
    device, VK_NULL_HANDLE, 1, &pipeline_info, NULL, &priv->pipeline);

  vkDestroyShaderModule (device, vs_module, NULL);
  vkDestroyShaderModule (device, fs_module, NULL);

  vk_check_error ("vkCreateGraphicsPipelines", res, FALSE);

  return TRUE;
}

static void
normal_model_renderer_class_init (NormalModelRendererClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;

  GulkanSwapchainRendererClass *parent_class =
    GULKAN_SWAPCHAIN_RENDERER_CLASS (klass);
  parent_class->init_draw_cmd = _init_draw_cmd;
  parent_class->init_pipeline = _init_pipeline;
}


static void
_update_descriptor_sets (NormalModelRenderer *self,
                        VkImageView diffuse_texture_image_view,
                        VkImageView normal_texture_image_view)
{
  NormalModelRendererPrivate *priv = normal_model_renderer_get_instance_private (self);

  GulkanClient *client = gulkan_renderer_get_client (GULKAN_RENDERER (self));
  VkDevice device = gulkan_client_get_device_handle (client);

  vkUpdateDescriptorSets (
    device, 3,
    (VkWriteDescriptorSet[]){
      { .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstSet = priv->descriptor_set,
        .dstBinding = 0,
        .dstArrayElement = 0, 
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .pTexelBufferView = NULL,
        .pBufferInfo =
          &(VkDescriptorBufferInfo){
            .buffer =
              gulkan_uniform_buffer_get_handle (priv->transformation_ubo),
            .offset = 0,
            .range = VK_WHOLE_SIZE,
          } 
      },
      {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstSet = priv->descriptor_set,
        .dstBinding = 1,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .pImageInfo = &(VkDescriptorImageInfo) {
          .sampler = priv->diffuse_sampler,
          .imageView = diffuse_texture_image_view,
          .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        },
        .pTexelBufferView = NULL
      },
      {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstSet = priv->descriptor_set,
        .dstBinding = 2,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .pImageInfo = &(VkDescriptorImageInfo) {
          .sampler = priv->normal_sampler,
          .imageView = normal_texture_image_view,
          .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        },
        .pTexelBufferView = NULL
      } },
    0, NULL);
}

gboolean
normal_model_renderer_initialize (NormalModelRenderer    *self,
                           VkSurfaceKHR      surface,
                           VkClearColorValue clear_color,
                           ShaderResources  *resources,
                           GulkanTexture    *diffuse_texture,
                           GulkanTexture    *normal_texture)
{
  NormalModelRendererPrivate *priv = normal_model_renderer_get_instance_private (self);
  GulkanClient *client = gulkan_renderer_get_client (GULKAN_RENDERER (self));
  GulkanDevice *gulkan_device = gulkan_client_get_device (client);

  priv->transformation_ubo =
    gulkan_uniform_buffer_new (gulkan_device, sizeof (Transformation));
  if (!priv->transformation_ubo)
    return FALSE;

  VkDescriptorSetLayoutBinding bindings[] = {
    {
      .binding = 0,
      .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
      .descriptorCount = 1,
      .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
      .pImmutableSamplers = NULL,
    },
    {
      .binding = 1,
      .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      .descriptorCount = 1,
      .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
    },
    {
      .binding = 2,
      .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      .descriptorCount = 1,
      .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
    }
  };

  uint32_t set_count = 1;
  VkDescriptorPoolSize pool_sizes[] = {
    {
      .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
      .descriptorCount = set_count,
    },
    {
      .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      .descriptorCount = set_count,
    },
    {
      .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      .descriptorCount = set_count,
    }
  };

  VkDevice device = gulkan_device_get_handle (gulkan_device);
  priv->descriptor_pool = GULKAN_DESCRIPTOR_POOL_NEW (device, bindings,
                                                      pool_sizes, set_count);
  if (!priv->descriptor_pool)
    return FALSE;

  if (!gulkan_descriptor_pool_allocate_sets (priv->descriptor_pool, 1,
                                            &priv->descriptor_set))
    return FALSE;

  if (!_init_texture_sampler (device, &priv->diffuse_sampler))
    return FALSE;

  VkImageView vk_diffuse_image_view = gulkan_texture_get_image_view (diffuse_texture);

  if (!_init_texture_sampler (device, &priv->normal_sampler))
    return FALSE;

  VkImageView vk_normal_image_view = gulkan_texture_get_image_view (normal_texture);

  _update_descriptor_sets (self, vk_diffuse_image_view, vk_normal_image_view);

  if (!gulkan_swapchain_renderer_initialize (GULKAN_SWAPCHAIN_RENDERER (self),
                                             surface, clear_color,
                                             (gpointer) resources))
    return FALSE;

  return TRUE;
}

void
normal_model_renderer_update_ubo (NormalModelRenderer *self, gpointer ubo)
{
  NormalModelRendererPrivate *priv = normal_model_renderer_get_instance_private (self);
  gulkan_uniform_buffer_update (priv->transformation_ubo, ubo);
}
