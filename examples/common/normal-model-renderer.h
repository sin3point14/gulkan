/*
 * gulkan
 * Copyright 2020 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef NORMAL_MODEL_RENDERER_H_
#define NORMAL_MODEL_RENDERER_H_

#include <glib-object.h>
#include <gulkan.h>

G_BEGIN_DECLS

#define NORMAL_MODEL_TYPE_RENDERER normal_model_renderer_get_type()
G_DECLARE_DERIVABLE_TYPE (NormalModelRenderer, normal_model_renderer,
                          NORMAL_MODEL, RENDERER, GulkanSwapchainRenderer)

struct _NormalModelRendererClass
{
  GulkanSwapchainRendererClass parent;

  void
  (*init_draw_cmd) (NormalModelRenderer  *self,
                    VkCommandBuffer cmd_buffer);
};

G_END_DECLS

typedef struct
{
  float mv_matrix[16];
  float mvp_matrix[16];
  float normal_matrix[12];
} Transformation;

typedef struct {
  const gchar *vert;
  const gchar *frag;
} ShaderResources;

gboolean
normal_model_renderer_initialize (NormalModelRenderer    *self,
                           VkSurfaceKHR      surface,
                           VkClearColorValue clear_color,
                           ShaderResources  *resources,
                           GulkanTexture    *diffuse_texture,
                           GulkanTexture    *normal_texture);

void
normal_model_renderer_update_ubo (NormalModelRenderer *self, gpointer ubo);

#endif /* NORMAL_MODEL_RENDERER_H_ */
